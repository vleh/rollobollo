﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    private int count;

    public Text text;

    private void Start()
    {
        count = 0;
        updateCounter();
    }

    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(horizontal, 0, vertical);

        GetComponent<Rigidbody>().AddForce(direction * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "pickup")
        {
            Destroy(other.gameObject);
            count++;

            updateCounter();
        }
    }

    void updateCounter ()
    {
        text.text = "Score: " + count;
    }
}
